---
layout: archive
title: "Thank you!"
permalink: /thanks/
---

<script type="text/javascript">
window.setTimeout(function(){window.location.href = "/";}, 10000);</script>

Thank you for donating! Your donation will soon be reflected on the
[Donate](/donate/) page.