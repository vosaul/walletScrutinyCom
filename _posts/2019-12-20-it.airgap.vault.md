---
title: "AirGap Vault - Tezos, Cosmos, Ethereum, Bitcoin"
altTitle: 

users: 5000
appId: it.airgap.vault
launchDate: 2018-08-06
latestUpdate: 2020-04-24
apkVersionName: "3.2.0"
stars: 3.9
ratings: 36
reviews: 15
size: 8.0M
website: https://airgap.it/
repository: https://github.com/airgap-it/airgap-vault
issue: https://github.com/airgap-it/airgap-vault/issues/13
icon: it.airgap.vault.png
bugbounty: 
verdict: reproducible # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-04-28
reviewStale: false
signer: 486381324d8669c80ca9b8c79d383dc972ec284227d65ebfe9e31cad5fd3f342
reviewArchive:
- date: 2020-04-09
  version: "3.1.0"
  apkHash: f6f2f37ef164a585ae5de0ff28d5beaf729c8e41495ce1525af7b7642e1f963a
  gitRevision: b54542a17c2b75f5bd5ccbae353201d6f742bb50
  verdict: reproducible
- date: 2020-01-06
  version: "3.0.0"
  apkHash: d3bb8f0c7c30119405ef9b6c00ca5574e89da76d8ca5208aecc3530bf24e1987
  gitRevision: 63cf4944a2aaa3275258632dc3e7efbd957e3a89
  verdict: reproducible
- date: 2019-12-29
  version: "3.0.0"
  apkHash: d3bb8f0c7c30119405ef9b6c00ca5574e89da76d8ca5208aecc3530bf24e1987
  gitRevision: 1b2995ed2db18e2517812f7fbb3b2aca04a4653e
  verdict: nonverifiable

providerTwitter: AirGap_it
providerLinkedIn: 
providerFacebook: 
providerReddit: AirGap

permalink: /posts/it.airgap.vault/
redirect_from:
  - /it.airgap.vault/
---


Our
[test script](https://gitlab.com/walletscrutiny/walletScrutinyCom/-/blob/master/test.sh).
came to this conclusion:

```
Results for 
appId:          it.airgap.vault
apkVersionName: 3.2.0
apkVersionCode: 18088
apkHash:        951ee71325f9cee9237cc43235cd653363bf0d7f268e574e4b50856207c170e4

Diff:
Files /tmp/fromPlay_it.airgap.vault_18088/apktool.yml and /tmp/fromBuild_it.airgap.vault_18088/apktool.yml differ
Files /tmp/fromPlay_it.airgap.vault_18088/original/META-INF/MANIFEST.MF and /tmp/fromBuild_it.airgap.vault_18088/original/META-INF/MANIFEST.MF differ
Only in /tmp/fromPlay_it.airgap.vault_18088/original/META-INF: PAPERS.RSA
Only in /tmp/fromPlay_it.airgap.vault_18088/original/META-INF: PAPERS.SF
```

which is what we want to see for the verdict **reproducible**.

The huge docker image that needs to
be recreated for each build feels like an overkill and might raise other issues
once somebody actually does a code audit.