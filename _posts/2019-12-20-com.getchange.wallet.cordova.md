---
title: "Change Invest: Buy & Sell Bitcoin Commission-free"
altTitle: 

users: 50000
appId: com.getchange.wallet.cordova
launchDate: 
latestUpdate: 2020-06-30
apkVersionName: "10.8.95"
stars: 4.3
ratings: 796
reviews: 399
size: 29M
website: https://getchange.com
repository: 
issue: 
icon: com.getchange.wallet.cordova.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: changefinance
providerLinkedIn: company/changeinvest
providerFacebook: changeinvest
providerReddit: 

permalink: /posts/com.getchange.wallet.cordova/
redirect_from:
  - /com.getchange.wallet.cordova/
---


On their Google Play description we find

> • Secure: Funds are protected in multi-signature, cold-storage cryptocurrency
  wallets

which means it is a custodial service and thus **not verifiable**.