---
title: "Bitcoin Wallet Totalcoin - Buy and Sell Bitcoin"
altTitle: 

users: 100000
appId: io.totalcoin.wallet
launchDate: 2018-04-01
latestUpdate: 2020-05-25
apkVersionName: "4.5.5"
stars: 4.3
ratings: 3483
reviews: 1784
size: 10M
website: http://totalcoin.io/
repository: 
issue: 
icon: io.totalcoin.wallet.png
bugbounty: 
verdict: nosource # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-11-23
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/2019/11/totalcoin/
redirect_from:
  - /totalcoin/
  - /io.totalcoin.wallet/
---


On the wallet's description we read:

> Your Bitcoin, Ethereum and Bitcoin Cash are securely hidden in your blockchain
wallet and always under your control.

which is the most "explicit" hint at the wallet being non-custodial.

On their website we find not much about the wallet apart from a link to Google
Play.

On GitHub we
[find no hits searching for their application ID](https://github.com/search?q="io.totalcoin.wallet").

Our verdict: This wallet is **not verifiable**.
